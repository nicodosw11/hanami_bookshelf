module Web
  module Controllers
    module Activities
      class Index
        include Web::Action

        expose :activities

        def call(_)
          #@activities = ActivityRepository.new.newest_to_oldest # => ROM::Relation::Composite
          @activities = ActivityRepository.new.newest_to_oldest.to_a # => I need an array!
        end
      end
    end
  end
end
