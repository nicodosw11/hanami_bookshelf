module Web
  module Controllers
    module Books
      class Index
        include Web::Action

        expose :book_db, :books

        def call(_)
          @book_db = BookRepository.new
          @books = BookRepository.new.all
        end
      end
    end
  end
end
