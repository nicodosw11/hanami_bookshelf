module Web
  module Controllers
    module Books
      class Create
        include Web::Action

        expose :book

        params do
          required(:book).schema do
            required(:title).filled(:str?)
            required(:author).filled(:str?)
          end
        end

        def call(params)
          if params.valid?
            # @book = BookRepository.new.create(params[:book])
            title, author = params[:book].to_h.values_at(:title, :author)
            result = Book::Create.new(title, author).call
            @book = result.book

            redirect_to routes.books_path
          else
            self.status = 422
          end
        end
      end
    end
  end
end
