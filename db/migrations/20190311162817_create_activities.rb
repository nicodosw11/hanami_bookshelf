Hanami::Model.migration do
  change do
    create_table :activities do
      primary_key :id

      column :timestamp,  Time,     null: false
      column :operation,  String,   null: false
      column :old_title,  String
      column :old_author, String
      column :new_title,  String
      column :new_author, String

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end
  end
end
