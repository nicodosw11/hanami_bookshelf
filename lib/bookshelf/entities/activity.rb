class Activity < Hanami::Entity
  module CRUD
    CREATE = 'create'.freeze
    UPDATE = 'update'.freeze
    DELETE = 'delete'.freeze
  end
end
