require 'hanami/interactor'

class Book::Create
  include Hanami::Interactor

  expose :activity, :book

  def initialize(title, author)
    @book = Book.new(title: title, author: author)
    @activity = Activity.new(
      timestamp: Time.now,
      operation: Activity::CRUD::CREATE,
      new_title: title,
      new_author: author
    )
  end

  def call
    BookRepository.new.transaction do
      @book = BookRepository.new.create(@book)
      @activity = ActivityRepository.new.create(@activity)
    end
  end
end
