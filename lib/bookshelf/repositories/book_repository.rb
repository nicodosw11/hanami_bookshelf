class BookRepository < Hanami::Repository
  def count_all
    books.count
  end

  def most_recent_books(limit = 10)
    books
    .order(Sequel.desc(:books__created_at))
    .limit(limit)
    .pluck(:title)
  end

  def most_recent_by_author(author, limit: 5)
    books
    .where(author: author)
    .order(:created_at)
    .limit(limit)
    .pluck(:title)
  end
end
