class ActivityRepository < Hanami::Repository
  def newest_to_oldest
    activities.order(:timestamp)
    # activities.order(Sequel.desc(:activities__timestamp))
  end
end
