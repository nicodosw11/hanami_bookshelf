RSpec.describe ActivityRepository, type: :repository do
  let(:repository) { ActivityRepository.new }
  def create_activity(timestamp)
    repository.create(
      Activity.new(
        timestamp: timestamp,
        operation: Activity::CRUD::CREATE
      )
    )
  end

  before do
    repository.clear

    @activity_1 = create_activity(Time.now - 1)
    @activity_2 = create_activity(Time.now)
  end

  describe '#newest_to_oldest' do
    it 'provides the activities in the correct order' do
      result = repository.newest_to_oldest.to_a
      expect(result).to eq([@activity_1, @activity_2])
    end
  end
end


