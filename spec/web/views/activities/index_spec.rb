require "spec_helper"

RSpec.describe Web::Views::Activities::Index, type: :view do
  # let(:exposures) { Hash[format: :html] }
  let(:exposures) { Hash[foo: 'bar'] }
  let(:template)  { Hanami::View::Template.new('apps/web/templates/activities/index.html.erb') }
  let(:view)      { described_class.new(template, exposures) } #let(:view)      { Web::Views::Activities::Index.new(template, exposures) }
  let(:rendered)  { view.render }

  # it 'exposes #format' do
  #   expect(view.format).to eq exposures.fetch(:format)
  # end

  it 'exposes #foo' do
    expect(view.foo).to eq exposures.fetch(:foo)
  end
end
