require "spec_helper"

RSpec.describe Web::Controllers::Activities::Index, type: :action do
  let(:action) { described_class.new } #let(:action) { Web::Controllers::Activities::Index.new }
  let(:params) { Hash[] }
  let(:repository) { ActivityRepository.new}

  before do
    repository.clear

    @activity = repository.create(
      Activity.new(
        timestamp: Time.now,
        operation: Activity::CRUD::CREATE,
        new_title: 'TDD',
        new_author: 'Kent Beck'
      )
    )
  end

  it 'is successful' do
    response = action.call(params)
    expect(response[0]).to eq 200
  end

  it 'exposes all activities' do
    action.call(params)
    expect(action.exposures[:activities].to_a).to eq([@activity])
  end
end
